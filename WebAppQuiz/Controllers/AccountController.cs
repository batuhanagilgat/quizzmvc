﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebAppQuiz.Models;
using WebAppQuiz.ViewModel;

namespace WebAppQuiz.Controllers
{
    public class AccountController : Controller
    {
        private QuizDBEntities objQuizDbEntities;

        public AccountController()
        {
            objQuizDbEntities = new QuizDBEntities();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(AdminViewModel objAdminViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Admin obAdmin =
                        objQuizDbEntities.Admins.SingleOrDefault(model => model.UserName == objAdminViewModel.UserName);
                    if (obAdmin == null)
                    {
                        ModelState.AddModelError(String.Empty, "Email is not exists.");
                    }
                    else if (obAdmin.UserPassword != objAdminViewModel.UserPassword)
                    {
                        ModelState.AddModelError(String.Empty, "Email & is not exists.");
                    }
                    else
                    {
                        FormsAuthentication.SetAuthCookie(objAdminViewModel.UserName, false);
                        var authTicket = new FormsAuthenticationTicket(1, obAdmin.UserName, DateTime.Now, DateTime.Now.AddMinutes(20),
                            false, "Admin");

                        string encryptTicket = FormsAuthentication.Encrypt(authTicket);

                        var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptTicket);
                        HttpContext.Response.Cookies.Add(authCookie);
                        return RedirectToAction("Index", "Admin");
                    }
                }
            }
            catch (DivideByZeroException e)
            {
                Console.Write(e.Message);
                Console.ReadLine();
                throw;
            }
            
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}