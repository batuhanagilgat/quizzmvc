﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppQuiz.Models;
using WebAppQuiz.ViewModel;

namespace WebAppQuiz.Controllers
{
    [Authorize]
    public class AdminController:Controller
    {
        private QuizDBEntities objQuizDbEntities;

        public AdminController()
        {
            objQuizDbEntities = new QuizDBEntities();
        }

        public ActionResult Index()
        {
            CategoryViewModel objCategoryViewModel = new CategoryViewModel();
            objCategoryViewModel.ListOfCategory = (from objCat in objQuizDbEntities.Categories
                                                   select new SelectListItem()
                                                   {
                                                       Value = objCat.CategoryId.ToString(),
                                                       Text = objCat.CategoryName
                                                   }).ToList();
             
                                                return View(objCategoryViewModel);
        }

        [HttpPost]
        public JsonResult Index(QuestionOptionViewModel QuestionOption )
        {
            Question objQuestion = new Question();
            objQuestion.QuestionName = QuestionOption.QuestionName;
            objQuestion.CategoryId = QuestionOption.CategoryId;
            objQuestion.IsActive = true; // seçileni aktif et.
            objQuestion.IsMultiple = false; // çoklu seçimi false yap.
            objQuizDbEntities.Questions.Add(objQuestion);
            objQuizDbEntities.SaveChanges();
            int questionId = objQuestion.QuestionId;

            foreach (var item in QuestionOption.ListOfOptions)
            {
                Option objOption = new Option();
                objOption.OptionName = item;
                objOption.QuestionId = questionId;
                objQuizDbEntities.Options.Add(objOption);
                objQuizDbEntities.SaveChanges();

            }
            Answer objAnswer = new Answer();    
            objAnswer.AnswerText = QuestionOption.AnswerText;
            objAnswer.QuestionId = questionId;
            objQuizDbEntities.Answers.Add(objAnswer);
            objQuizDbEntities.SaveChanges();


            return Json(new { message="Soru başarıyla eklendi.", success = true}, JsonRequestBehavior.AllowGet);

        }
    }
}