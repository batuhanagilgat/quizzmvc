﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebAppQuiz.Models;
using WebAppQuiz.ViewModel;

namespace WebAppQuiz.Controllers
{
    public class QuizController : Controller
    {
        private QuizDBEntities objQuizDbEntities;


        public QuizController()
        {
            objQuizDbEntities = new QuizDBEntities();
        }
        public ActionResult Index()
        {
            QuizCategoryViewModel objQuizCategoryViewModel = new QuizCategoryViewModel();   
            objQuizCategoryViewModel.ListOfCategory = (from obj in objQuizDbEntities.Categories
                                                       select new SelectListItem()
                                                       {
                                                           Value = obj.CategoryId.ToString(),
                                                           Text = obj.CategoryName

                                                       }).ToList();
            return View(objQuizCategoryViewModel);
        }

        [HttpPost]
        public ActionResult Index(string CandidateName,int CategoryId)
        {
            User objUser = new User();
            objUser.UserName = CandidateName;
            objUser.LoginTime = DateTime.Now;
            objUser.CategoryId = CategoryId;
            objQuizDbEntities.Users.Add(objUser);
            objQuizDbEntities.SaveChanges();

            Session["CandidateName"] = CandidateName;
            Session["CategoryId"] = CategoryId;
            return View("QuestionIndex"); 

        }

        public PartialViewResult UserQuestionAnswer()
        {
            int pageSize = 1;
            int pageNumber = 0;
            int CategoryId = Convert.ToInt32(Session["CategoryId"]); 
            if(Session["CadQuestionAnswer"] == null)
            {
                pageNumber = pageNumber + 1;
            }
            else
            {
                List<CandidateAnswer> canAnswer = Session["CadQuestionAnswer"] as List<CandidateAnswer>;
                pageNumber = canAnswer.Count + 1;
            }
            List<Question> listOfQuestion = new List<Question>();
            listOfQuestion = objQuizDbEntities.Questions.Where(model => model.CategoryId == CategoryId).ToList();

            QuizQuestionAnswerViewModel objAnswerViewModel = new QuizQuestionAnswerViewModel();
            Question objQuestion = new Question();
            objQuestion = listOfQuestion.Skip((pageNumber - 1) * pageSize).Take(pageSize).FirstOrDefault();

            objAnswerViewModel.QuestionId = objQuestion.QuestionId;
            objAnswerViewModel.QuestionName = objQuestion.QuestionName;
            objAnswerViewModel.ListOfQuizOptions = (from obj in objQuizDbEntities.Options
                                                    where obj.QuestionId == objQuestion.QuestionId
                                                    select new QuizOption()
                                                    {
                                                        OptionName = obj.OptionName,
                                                        OptionId = obj.OptionId
                                                    }).ToList();
            return PartialView("QuizQuestionOptions", "data");
        }

    }
}