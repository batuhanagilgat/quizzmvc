﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
namespace WebAppQuiz.ViewModel
{
    public class QuizCategoryViewModel
    {
        [Display(Name = "Category")]
        [Required(ErrorMessage ="Hatalı bir giriş yaptınız.")]
        public int CategoryId{get;set;}

        [Display(Name = "Candidate")]
        [Required(ErrorMessage = "Hatalı bir giriş yaptınız.")]
        public string CandidateName {get;set;}

        public IEnumerable<SelectListItem> ListOfCategory { get; set; }
        // listof boş geliyor. syntax hatası yok.
        // s
    }
}