﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace WebAppQuiz.ViewModel
{
    public class CategoryViewModel
    {

        [Display(Name ="Kategori")]
        [Required(ErrorMessage ="Kategori kullanılıyor.")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage ="Soru kullanılmakta")]
        [Display(Name ="Soru")]
        public string QuestionName { get; set; }

        public string OptionName { get; set; }

        public string CategoryName { get; set; }

        public IEnumerable<SelectListItem> ListOfCategory { get; set; }
        

    }
}