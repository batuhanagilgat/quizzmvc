﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAppQuiz.ViewModel
{
    public class AdminViewModel
    {
        [Display(Name="Email")]
        [Required(ErrorMessage ="Hatalı bir e-mail")]
        [EmailAddress(ErrorMessage ="Mail giriniz")]
        public string UserName { get; set; }


        [Display(Name = "Password")]
        [Required(ErrorMessage = "Hatalı bir parola")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }


    }
}